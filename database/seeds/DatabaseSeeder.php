<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddDefaultLanguagesSeeder::class);
        $this->call(DefaultUserSeeder::class);
        $this->call(AddDefaultStaticTranslatesSeeder::class);
    }
}
