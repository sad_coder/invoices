<?php

use App\Models\Language;
use Illuminate\Database\Seeder;

class AddDefaultLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defautlLangCodes = ['ru','en'];
        $defaultLangNames = ['russian','english'];
        $defaultLangIcons = [Language::ICON_DIR_PATH . '/'.'ru.png',Language::ICON_DIR_PATH . '/'.'en.png'];
        $insertData = [];
        foreach ($defautlLangCodes as $k => $langCode) {
            $insertData[$k]['code'] = $langCode;
            $insertData[$k]['name'] = $defaultLangNames[$k];
            $insertData[$k]['icon'] = $defaultLangIcons[$k];
        }
        Language::query()->insert($insertData);
    }
}
