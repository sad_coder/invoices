<?php

use App\Models\Language;
use App\Models\StaticData;
use App\Models\StaticDataTranslate;
use Illuminate\Database\Seeder;

class AddDefaultStaticTranslatesSeeder extends Seeder
{

    private $ruTranslate;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultStatics = ['edit','Delete','add new','choose language','Create','No','Supply','Comment','Actions','Are you sure you want to delete?','yes','not'];
        $defInsertData = [];
        foreach ($defaultStatics as $k => $defaultStatic) {
            $defInsertData[$k]['name'] = $defaultStatic;
            $defInsertData[$k]['slug'] = Str::slug($defaultStatic,'-',config('app.default_lang_code'));
        }
        StaticData::query()->insert($defInsertData);

        $insertTranslates = [];
        $ruLang = Language::query()->where('code','ru')->first();
        $enLang = Language::query()->where('code','en')->first();
        $languages = ['ru'=> $ruLang,'en'=>$enLang];
        $this->ruTranslate = [
            'edit' => 'Редактировать',
            'delete' =>'Удалить',
            'add-new'=>'добавить новый',
            'choose-language'=>'выбрать язык',
            'create' => 'Создать',
            'no'=>'номер',
            'supply' => 'поставка',
            'comment'=>'комментарий',
            'actions'=> 'действия',
            'are-you-sure-you-want-to-delete'=> 'Вы действительно хотите удалить?',
            'yes'=> 'Да',
            'not'=>'Нет'
        ];

        $staticData = StaticData::query()->get();
        $key = 0;
        foreach ($staticData as $k => $item) {
            foreach ($languages as $lang) {
                $insertTranslates[$key]['name'] = $this->getTranslate($lang->code,$item);
                $insertTranslates[$key]['lang_id'] = $lang->id;
                $insertTranslates[$key]['static_data_id'] = $item->id;
                $key++;
            }
        }
        StaticDataTranslate::query()->insert($insertTranslates);
    }

    /**
     * @param $langCode
     * @param $dataItem
     * @return mixed
     */
    private function getTranslate($langCode, $dataItem){
        if ($langCode === 'ru') {
            return $this->ruTranslate[$dataItem->slug];
        }
        return $dataItem->name;
    }

}
