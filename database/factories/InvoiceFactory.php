<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Invoice;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'invoice_date' => $faker->dateTime(),
        'supply_date' => $faker->dateTime(),
        'user_id' => User::all()->random()->id,
        'comment' => Str::random(rand(10,26))
        ];
});
