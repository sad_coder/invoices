<?php

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Invoice::getTableName(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('invoice_date');
            $table->timestamp('supply_date');
            $table->unsignedBigInteger('user_id');
            $table->text('comment');
            $table->timestamps();
        });

        Schema::table(Invoice::getTableName(), function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on(User::getTableName())
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Invoice::getTableName(), function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists(Invoice::getTableName());
    }
}
