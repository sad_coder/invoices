<?php

use App\Models\Invoice;
use App\Models\InvoiceTranslate;
use App\Models\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InvoiceTranslate::getTableName(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('comment')->nullable();
            $table->unsignedBigInteger('lang_id');
            $table->unsignedBigInteger('invoice_id');
            $table->timestamps();
        });

        Schema::table(InvoiceTranslate::getTableName(), function (Blueprint $table) {
            $table->foreign('lang_id')
                ->references('id')
                ->on(Language::getTableName())
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('invoice_id')
                ->references('id')
                ->on(Invoice::getTableName())
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(InvoiceTranslate::getTableName(), function (Blueprint $table) {
            $table->dropForeign(['lang_id']);
        });

        Schema::dropIfExists(InvoiceTranslate::getTableName());
    }
}
