<?php

use App\Models\Language;
use App\Models\StaticData;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticDataTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_data_translates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('lang_id');
            $table->unsignedBigInteger('static_data_id');
            $table->timestamps();
        });
        Schema::table('static_data_translates', function (Blueprint $table) {
            $table->foreign('lang_id')->references('id')->on(Language::getTableName())->onDelete('cascade');
            $table->foreign('static_data_id')->references('id')->on(StaticData::getTableName())->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('static_data_translates', function (Blueprint $table) {
            $table->dropForeign(['lang_id']);
            $table->dropForeign(['static_data_id']);
        });
        Schema::dropIfExists('static_data_translates');
    }
}
