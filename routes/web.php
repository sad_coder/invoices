<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('invoice.index');
});

Route::resource('invoice', 'InvoiceController', ['except'=>'show','destroy']);
Route::post('delete.invoice','InvoiceController@destroy')->name('invoice.destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
