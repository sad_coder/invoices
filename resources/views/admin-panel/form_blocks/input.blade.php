<div class="form-group row">
    <label class="label-control col-md-5 col-sm-5">{{$label}} </label>
    <input max="{{$max ?? ''}}" name="{{$name}}" type="text" {{isset($disabled) ? 'disabled' : null}}
           {{$attr ?? ''}}  placeholder="{{$placeholder??''}}" value="{{$value ?? null}}"
           class="form-control {{$class ?? null}}" id="{{$id ?? null}}" autocomplete="off">
</div>

@if(isset($datePicker))
    <script>
        $(function () {
            $("#{{$id}}").datepicker({
                    dateFormat: "yy-mm-dd",
                    timeFormat:  "hh:mm:ss"
                });
            $("#{{$id}}").datepicker("show");
        });
    </script>
@endif
