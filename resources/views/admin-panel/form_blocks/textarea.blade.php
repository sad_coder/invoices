<div class="form-group row">
    <label class="label-control col-md-2 col-sm-2" for="first-name">{{$label}} </label>
    <div class="col-md-9 col-sm-9">
        <textarea maxlength="{{$maxlength ?? '' }}" name="{{$name}}" {{isset($required)?'required':''}} rows="5" class="form-control col-md-12 col-xs-12">{!! isset($value) ? $value : '' !!}</textarea>
    </div>
</div>
