<div id="deleteModalWindow">
    <div id="delete-modal-window-container">
        <div id="close-delete-modal-window-icon">
            <button id="delete-modal-window-close-button">
                <i style="margin-right: 5px" class="fa fa-window-close"></i>
            </button>
        </div>
        <div id="delete-modal-window-title">Вы действьтельно хотите удалить?</div>
        <div class="modal-icon-action">
            <button class="btn btn-success" id="DeletedTrue">Да</button>
            <button class="btn btn-danger" id="DeletedFalse">Нет</button>
        </div>
    </div>
</div>


<style>
    #deleteModalWindow {
        position: fixed;
        transform: scale(0);
        z-index: 99999;
        background: #4e4b4ba6;
        width: 36%;
        height: 24%;
        left: 30%;
        transition: .4s;
        top: 22%;
    }

    #delete-modal-window-container {
        padding: 10%;
        text-align: center;
        position: relative;
    }

    #delete-modal-window-title {
        font-size: 20px;
        font-weight: 900;
        color: white;
        text-shadow: 0px 0px 0px black;
        margin-bottom: 2%;
    }

    #close-delete-modal-window-icon {
        position: absolute;
        left: 98%;
        top: -3%;
        border-radius: 18px;
        text-align: center;
    }

    #delete-modal-window-close-button > i {
        color: red;
        transform: scale(1.9);
        transition: .4s;
    }

    #delete-modal-window-close-button:hover > i {
        color: #c82333;
        cursor: pointer;
    }
    button#delete-modal-window-close-button {
        outline: none;
        background: #f5deb300;
        border: none;
    }
    .pagination {
        margin-left: 40%;
    }

</style>

<script>

    $('#close-delete-modal-window-icon').click(function () {
        $('#deleteModalWindow').css('transform', 'scale(0)');
    });

    $('#DeletedFalse').click(function () {
        $('#deleteModalWindow').css('transform', 'scale(0)');
    });

    $('.deleteButton').click(function () {
        $('#deleteModalWindow').css('transform', 'scale(1)');
        $('#DeletedTrue').attr('data-delete-id',this.id)
    });
    $('#DeletedTrue').click(function () {
        let deletedItemId = this.getAttribute('data-delete-id');
        $.ajax({
            type: 'POST',
            url: "{{route('invoice.destroy')}}",
            data: {
                id: deletedItemId,
                "_token": "{{ csrf_token() }}",
            },

            success: function (response) {
                console.log(response);
                $('#table-item-id-'+response.deleted_item_id).remove();
                $('#deleteModalWindow').css('transform', 'scale(0)')
            },
        });

    })


</script>
