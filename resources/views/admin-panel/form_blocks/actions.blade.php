@can('update',$item)
<a href="{{$route_edit}}">
    <button class="btn btn-sm btn-primary" style="margin-bottom: 5px;"><i style="margin-right: 5px" class="fa fa-edit"></i>{{getStaticTranslateBySlug('edit',$staticData ?? 'Редактировать')}}</button>
</a>
@endcan
@can('delete',$item)
    <div class="delete">
            <button type="submit" class="btn btn-danger btn-sm deleteButton" id ={{$item->id}}>
                <i style="margin-right: 5px" class="fa fa-trash"></i> {{getStaticTranslateBySlug('delete',$staticData ?? 'Удалить')}}
            </button>
    </div>
@endcan

