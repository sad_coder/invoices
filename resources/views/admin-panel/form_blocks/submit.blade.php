<div class="form-actions center border-top-0">
    <button type="submit" class="btn btn-primary btn-min-width">
        <i class="fa fa-{{$fa ?? 'plus'}}"></i> {{ $title ?? 'Создать' }}
    </button>
</div>
