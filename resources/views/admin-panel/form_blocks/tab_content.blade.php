<div role="tabpanel" class="tab-pane {{$k==0?'active':''}}" id="tab{{($name??'').$k}}" aria-expanded="true" aria-labelledby="base-tab{{($name??'').$k}}">
    {{$slot}}
</div>
