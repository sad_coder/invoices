@extends('layouts.app')

@section('title')
    add invoice
@endsection

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-card-center">Add invoice</h4>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form class="form form-horizontal" action="{{ route('invoice.store') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <ul class="nav nav-tabs nav-topline">
                                    @foreach($languages as $k => $lang)
                                        @include('admin-panel.form_blocks.tab_nav',['k'=>$k,'code'=>$lang->code])
                                    @endforeach
                                </ul>
                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-5 ml-1 mt-100">
                                                @include('admin-panel.form_blocks.input',['label'=>'Number','name'=>'number','value' => $invoiceNumber,'disabled'=>true])
                                            </div>
                                            <div class="col-sm-5 ml-1 mt-100">
                                                @include('admin-panel.form_blocks.input',['label'=>'Invoice Date:','name'=>'invoice_date','datePicker' => true,'id' => 'invoice_date'])
                                            </div>
                                            <div class="col-sm-6 ml-1 mt-100">
                                                @include('admin-panel.form_blocks.input',['label'=>'Supply Date:','name'=>'supply_date','datePicker' => true, 'id' => 'supply_date'])
                                            </div>
                                        </div>
                                    </div>
                                    @foreach($languages as $k => $lang)
                                        @component('admin-panel.form_blocks.tab_content',['k'=>$k])
                                            @include('admin-panel.form_blocks.textarea',['label'=>'Comment','name'=>'trans_comment[]','index'=>$k])
                                        @endcomponent
                                    @endforeach
                                </div>
                            </div>
                            @include('admin-panel.form_blocks.submit',['title'=>'Save'])
                            @include('admin-panel.form_blocks.error')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
