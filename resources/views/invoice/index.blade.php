@extends('layouts.app')

@section('title')
    invoices
@endsection

@section('content')

    <section id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="title px-4">
                            <h4 class="card-title">Invoice</h4>
                        </div>
                        @auth
                            <div>
                                <a href="{{route('invoice.create')}}" type="button"
                                   class="btn btn-success btn-min-width"><i style="margin-right: 5px"
                                                                            class="fa fa-plus"></i>{{getStaticTranslateBySlug('add-new',$staticData)}}</a>
                            </div>
                        @endauth
                        <div class="dropdown ml-auto mr-3">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{getStaticTranslateBySlug('choose-language',$staticData)}}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                @foreach($languages as $lang)
                                    <a href="{{route('invoice.index',['lang' => $lang->code])}}" class="d-flex">
                                        <span class="lang px-2 mt-2">
                                            <img width="40px" height="40px" src="{{$lang->icon}}" alt="">
                                            {{$lang->name}}
                                        </span>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard table-responsive">
                            <table id="invoiceTable" class="table table-striped table-bordered sorted_table">
                                <thead>
                                <tr>
                                    <th>{{getStaticTranslateBySlug('create',$staticData)}}</th>
                                    <th>{{getStaticTranslateBySlug('no',$staticData)}}</th>
                                    <th>{{getStaticTranslateBySlug('supply',$staticData)}}</th>
                                    <th>{{getStaticTranslateBySlug('comment',$staticData)}}</th>
                                    @auth
                                        <th>{{getStaticTranslateBySlug('actions',$staticData)}}</th>
                                    @endauth
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr id="{{'table-item-id-'.$invoice->id}}">
                                        <td class="pk_id">{{ $invoice->invoice_date_in_correct_format }}</td>
                                        <td>{{ $invoice->number_invoice }}</td>
                                        <td>
                                            {{$invoice->supply_date_in_correct_format}}
                                        </td>
                                        <td>{{ $invoice->translates->first()->comment ?? $invoice->comment }}</td>
                                        @auth
                                            <td>
                                                @include('admin-panel.form_blocks.actions',['route_edit'=>route('invoice.edit',$invoice),'item' => $invoice])
                                            </td>
                                        @endauth
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination">
                                {!! $invoices->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    @include('admin-panel.form_blocks.delete-modal-window')
@endsection


