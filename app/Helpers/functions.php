<?php

use App\Helpers\Helper;

if (! function_exists('getStaticTranslateBySlug')) {
    function getStaticTranslateBySlug($slug,$staticTranslates)
    {
        return Helper::getStaticTranslateBySlug($slug,$staticTranslates);
    }
}
