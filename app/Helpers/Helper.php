<?php


namespace App\Helpers;


class Helper
{
    /**
     * @param $slug
     * @param $staticTranslates
     * @return mixed
     */
    public static function getStaticTranslateBySlug($slug, $staticTranslates)
    {
        return $staticTranslates->where('slug',$slug)->first()->translates->first()->name;
    }
}
