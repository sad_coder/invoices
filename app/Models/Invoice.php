<?php

namespace App\Models;
use App\Traits\EntityTableNameTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use EntityTableNameTrait;

    protected $guarded = ['id'];

    const ID_PREFIX = 'INV-0000';

    public function translates()
    {
        return $this->hasMany(InvoiceTranslate::class);
    }

    /**
     * @return Carbon
     */
    public function getInvoiceDateInCorrectFormatAttribute()
    {
        return Carbon::parse($this->invoice_date)->format('d-m-Y');
    }

    /**
     * @return Carbon
     */
    public function getSupplyDateInCorrectFormatAttribute()
    {
        return Carbon::parse($this->supply_date)->format('d-m-Y');
    }

    public function getNumberInvoiceAttribute()
    {
        return self::ID_PREFIX.''.$this->id;
    }
}
