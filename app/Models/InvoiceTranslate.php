<?php

namespace App\Models;

use App\Traits\EntityTableNameTrait;
use Illuminate\Database\Eloquent\Model;

class InvoiceTranslate extends Model
{
    use EntityTableNameTrait;

    protected $guarded = ['id'];
}
