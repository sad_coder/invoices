<?php

namespace App\Models;

use App\Traits\EntityTableNameTrait;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use EntityTableNameTrait;
    protected $guarded = ['id'];

    const ICON_DIR_PATH = 'storage/image/LanguageIcons';

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset($this->icon);
    }


}
