<?php

namespace App\Models;

use App\Traits\EntityTableNameTrait;
use Illuminate\Database\Eloquent\Model;

class StaticData extends Model
{
    use EntityTableNameTrait;
    protected $guarded = ['id'];

    public function translates()
    {
        return $this->hasMany(StaticDataTranslate::class);
    }


}
