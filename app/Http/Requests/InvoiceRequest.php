<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required|string',
            'supply_date' =>'required|date_format:Y-d-m',
            'invoice_date' =>'required|date_format:Y-d-m',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'comment.required' => 'Comment required field',
            'supply_date.required' => 'Supply date required field',
            'supply_date.date_format' => 'Supply date format d-m-Y',
            'invoice_date.date_format' => 'Invoice date format d-m-Y',
            'invoice_date.required' => 'Invoice date required field',
        ];
    }


    public function prepareForValidation()
    {
        $this->query->add([
            'comment' => $this->get('trans_comment')[0],
        ]);
    }

}
