<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceRequest;
use App\Models\Invoice;
use App\Models\Language;
use App\Models\StaticData;
use App\Models\StaticDataTranslate;
use App\Models\User;
use App\Services\InvoiceService;
use App\Traits\LanguageTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    use LanguageTrait, SoftDeletes;
    private $invoiceService;

    public function __construct()
    {
        $this->invoiceService = new InvoiceService();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        if ($request->user()->cant('create', Invoice::class)) {
            return redirect()->route('invoice.index');
        };
        $languages = Language::query()
            ->orderBy('id', 'asc')
            ->get(['id', 'code']);
        $invoiceNumber = Invoice::ID_PREFIX .''. (Invoice::query()->orderBy('id', 'desc')->value('id') + 1 );
        return view('invoice.create', compact('languages', 'invoiceNumber'));
    }

    /**
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InvoiceRequest $request)
    {
        if ($request->user()->cant('create', Invoice::class)) {
            return redirect()->route('invoice.index');
        };
        $invoice = Invoice::query()->create([
            'comment' => $request->get('comment'),
            'invoice_date' => $request->get('invoice_date') .' 00:00:00',
            'supply_date' => $request->get('supply_date') .' 00:00:00',
            'user_id' => $request->user()->id,
        ]);

        $this->invoiceService->addTranslates($request, $invoice);
        return redirect()->route('invoice.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $langId = $this->getLangId($request);
        $languages = Language::query()->get();
        $staticData = StaticData::query()->with([
            'translates' => function($q) use ($langId){
                $q->where('lang_id',$langId);
            }])->get();
        $invoices = Invoice::query()
            ->with([
                'translates' => function ($q) use ($langId) {
                    $q->where('lang_id', $langId);
                },
            ])
            ->orderBy('created_at', 'desc')
            ->select('id', 'invoice_date', 'supply_date', 'comment')
            ->paginate(10);

        return view('invoice.index', compact('invoices', 'languages','staticData'));
    }

    public function edit(Request $request, $id)
    {
        $invoice = Invoice::query()->whereId($id)->with('translates')->first();
        if ($request->user()->cant('update', $invoice)) {
            return redirect()->route('invoice.index');
        };

        $languages = Language::query()->orderBy('id', 'asc')->get();
        return view('invoice.edit', compact('invoice', 'languages'));
    }

    /**
     * @param InvoiceRequest $request
     * @param Invoice $invoice
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(InvoiceRequest $request, Invoice $invoice)
    {
        if ($request->user()->cant('update', $invoice)) {
            return redirect()->route('invoice.index');
        };
        $invoice->update([
            'invoice_date' =>Carbon::parse($request->get('invoice_date') .'00:00:00')->format('Y-m-d H:i:s'),
            'supply_date' => Carbon::parse($request->get('supply_date') .'00:00:00')->format('Y-m-d H:i:s'),
            'comment' => $request->get('comment'),
        ]);
        $this->invoiceService->addTranslates($request, $invoice);
        return redirect()->route('invoice.index');
    }


    /**
     * @param Request $request
     * @param Invoice $invoice
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $invoice = Invoice::query()->find($request->get('id'));

        if ($request->user()->cant('delete', $invoice)) {
            return false;
        };

        $deletedItemId = $invoice->id;
        $invoice->delete();
        return response()->json(['deleted_item_id' => $deletedItemId], JsonResponse::HTTP_OK);
    }


}
