<?php


namespace App\Traits;


use App\Models\Language;
use Illuminate\Http\Request;

trait LanguageTrait
{
    /**
     * @param Request $request
     * @return mixed
     */
    private function getLangId(Request $request)
    {
        return Language::query()
            ->where('code',$request->get('lang',config('app.default_lang_code')))
            ->value('id');
    }
}
