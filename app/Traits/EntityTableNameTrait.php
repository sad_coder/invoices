<?php


namespace App\Traits;


trait EntityTableNameTrait
{
    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
