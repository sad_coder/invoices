<?php


namespace App\Services;

use App\Models\Language;

class InvoiceService
{
    public function addTranslates($request,$invoice)
    {
        $languages = Language::query()
            ->orderBy('id','asc')
            ->get('id');
        for ($i = 0; $i < count($languages); $i++) {
            $invoice->translates()->updateOrCreate(
                [
                    'lang_id' => $languages[$i]->id,
                ],
                [
                    'comment' => $request['trans_comment'][$i] ?? null,
                ]
            );
        }
    }
}
